const gulp = require('gulp');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const cssnano = require('cssnano');
// const pxtorem = require ('postcss-pxtorem');
const rename = require('gulp-rename');
// const concat = require('gulp-concat');
const uglify = require('gulp-uglify-es').default;


const options = {
    // propList: ['font', 'font-size', 'line-height', 'letter-spacing', '*height', '*width', 'padding*', 'padding', '*position*', 'margin*', 'border*']
    propList: ['*']
};

gulp.task('sass', function() {
    return gulp.src('scss/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('css'))
});

// gulp.task('js', function () {
//     return gulp.src('js/*.js')
//         .pipe(concat('main.min.js'))
//         .pipe(uglify())
//         .pipe(gulp.dest('js'));
// });

gulp.task('watch', function() {
    gulp.watch('scss/*.scss', ['sass'])
});

gulp.task('default', ['watch']);