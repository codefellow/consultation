$(document).ready(function () {
    $(document).on('click', '.fieldset-submit', function (e) {
        var $fieldset = $(this).parents('.fieldset');
        // e.preventDefault();
        $fieldset.hide().next().delay(350).show();
        $(this).parent().next().hide();
    });

    $(document).on('click', '.back', function (e) {
        e.preventDefault();
        $(this).parents('fieldset').hide().prev().show().find('.back-next').show();

    });

    $(document).on('click', '.reset', function (e) {
        location.reload();

    });

    $(document).on('click', '.section h3' ,function () {
        $(this).parents('.form-row').find('.section, .section-info').addClass('active');
        $(this).parents('.form-row').siblings().find('.section:not(.login), .section-info').removeClass('active');
    });

    $('.doctors-list .item .avatar').click(function () {
        $(this).parent().addClass('active');
        $(this).parent().siblings().removeClass('active');
        $(this).parent().siblings().removeClass('default');

    });

    $('.doctors-list .item-default .avatar').click(function () {
        $(this).parent().addClass('active');
        $(this).parent().siblings().removeClass('active');
    });


    $('.doctors-list .item .rating').click(function () {
        $('.popup').attr('style', 'display: block !important');

    });


    $('.doctors-list .item-default .name').click(function () {
        $('.popup-default').attr('style', 'display: block !important');
    });



    $(document).on('keyup change', 'input#discount', function (e) {
        if($(this).val()!=""){
            $('.discount-submit').addClass('active');
            return false;
        }
    });

    $('.column').on('click', '.slot', function(e) {
        e.preventDefault()
        var target = $(e.target);

        target.addClass('active');
        target.siblings().removeClass('active');
        target.parent().siblings().find('.slot').removeClass('active');
        $('#tab3 .button').addClass('active');
    });


    $(document).on('click', '.edit', function (e) {
        e.preventDefault();
        $(this).parents('.section').removeClass('visited').addClass('active');
        $(this).parents('.form-row').find('.submited-info').hide();
        $(this).parents('.form-row').find('.section-info').addClass('active');
        $(this).parents('.form-row').siblings().find('.section-info').removeClass('active');
        $(this).parents('.interview').find('fieldset:last-of-type, .progres, .back-next, .skip').show();
    });

    $(document).on('click', '.doctor-choice .edit', function (e) {
        $('.methods').removeClass('visited');
        $('.submited-info.filled-question').removeClass('filled-question');
        $(this).parents('.form-row').siblings().find('.methods .submited-info').hide();
    });

    $(document).on('click', '.question .section-submit', function (e) {
        $(this).parents('.form-row').siblings().find('.file').addClass('active');
    });


    $('.fieldset:last-of-type .fieldset-submit').click(function (e) {
        e.preventDefault();
        $(this).parents('.section').removeClass('active').addClass('visited');
        $(this).parents('.form-row').next().find('.section').addClass('active')
    });

    $(document).on('click', '.section-submit', function (e) {
        e.preventDefault();
        $(this).parents('.form-row').find('.section').removeClass('active').addClass('visited');
        $(this).parents('.form-row').next().find('.section').addClass('active');
        $(this).parents('.form-row').find('.submited-info').show();
        $(this).parents('.form-row').find('.section-info.active').removeClass('active');
        $(this).parents('.form-row').next().find('.section-info').addClass('active');
        $('.progres, fieldset, .back-next, .reset, .skip').hide();
        $('html, body').animate({
            scrollTop: $('.section.active').offset().top - 20
        }, 'slow');
    });

    $(document).on('click', '.drop-toggle .toggler', function (e) {
        e.preventDefault();
        $(this).parent().parent().find('.drop-menu').toggleClass('open');
        $(this).parent().parent().siblings().find('.drop-menu').removeClass('open');

    });

    $(document).mouseup(function(e)
    {
        var container = $(".drop-menu.open");
        var container2 = $(".popup, .popup-default");

        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0)
        {
            container.removeClass('open');
        }

        if (!container2.is(e.target) && container2.has(e.target).length === 0)
        {
            container2.hide();
        }
    });

    $(document).on('click', '.popup .close', function () {
        $(this).parent().hide();
    });

    $(document).on('click', '.drop-menu .item', function (e) {
        $(this).parents('.drop-toggle').find('.toggler').html($(this).html());
        $('.drop-menu.open').removeClass('open');
    });

    $(document).on('click', '.doctors-list-toggle', function (e) {
        var oldText = $(this).text();
        var newText = $(this).data('text');
        $(this).text(newText).data('text',oldText);
        $('.doctors-list').toggleClass('open');
        $(this).toggleClass('open');
        $('html, body').animate({
            scrollTop: $(this).offset().top - 20
        }, 'slow');
    });

    $(document).on('click', '.file .section-submit', function (e) {
        var oldText = $(this).text();
        var newText = $(this).data('text');
        $(this).text(newText).data('text',oldText);
        $('.doctors-list').toggleClass('open');
        $(this).toggleClass('open');
    });

    $('.login .section-submit').click(function (e) {
        $(this).parents('.login').find('.payment').addClass('active');
        $('html, body').animate({
            scrollTop: $('.login').offset().top - 20
        }, 'slow');
    });


    $(document).on('click', '.have-code p', function () {
        $(this).parent().hide();
        $(this).parents('.code').find('.code-active').show();
        $('.discount-submit').show();

    });

    $(document).on('click', '.remove-element-file', function () {
        $(this).parent().remove();
        $('ul#uploaded2').html($('ul#uploaded').html());
        $('ul#uploaded').html($('ul#uploaded2').html());
    });


    $("input#question").change(function() {
        var heading_text = $('.methods h3');
        var oldText2 = $(heading_text).text();
        var newText2 = $(heading_text).data('text');
        var newClass = $('.question').data('class');
        $(heading_text).text(newText2).data('text',oldText2);
        if(this.checked)
        {
            $('.question').show();
            $('.contact-method').hide();
            $('.submited-info').addClass('filled-question');
            $('.date').parent().parent().hide();
            $('.doctors-list-toggle').hide();
            $('.doctors-list').removeClass('open');
            $('.doctor-choose .drop-toggle .toggler .doctors').hide();
            $('.doctor-choose .drop-toggle .toggler .questions').removeClass('d-none');

        }else {
            $('.question').hide();
            $('.contact-method').show();
            $('.submited-info').removeClass('filled-question');
            $('.date').parent().parent().show();
            $('.doctors-list-toggle').show();
            $('.doctor-choose .drop-toggle .toggler .doctors').show();
            $('.doctor-choose .drop-toggle .toggler .questions').addClass('d-none');


        }
    });


    $('.contact-method .item ').click(function (e) {
       $(this).parents('.method').find('.submited-info .item .icon').html($(this).parent('.item .icon').html);
        $(this).parents('.method').find('.submited-info .item p').html($(this).parent('.item p').html);
    });

    $(document).on('click', '.methods .section-submit', function () {
        $('.question-submited-content p.box').text($('.question .content #question-field').val());
        $(this).parents('.doctor-choice').find('.doctors-list-toggle').removeClass('open').text('Wybierz lekarza');
        $(this).parents('.doctor-choice').find('.doctors-list').removeClass('open');

    });

    $('.days-row').slick({
        slidesToShow: 7,
        nextArrow: '<img src="img/arrow-next.svg" class="right slick-next">',
        prevArrow: '<img src="img/arrow-prev.svg" class="left slick-prev">',
        asNavFor: '.slots-section',
        infinite: false,
        responsive: [
            {
                breakpoint: 1600,
                settings: {
                    slidesToShow: 6
                }
            },
            {
                breakpoint: 1366,
                settings: {
                    slidesToShow: 5
                },
            },
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4
                },
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 3
                },
            }
        ]

    });

    $('.slots-section').slick({
        slidesToShow: 7,
        arrows: false,
        asNavFor: '.days-row',
        infinite: false,
        responsive: [
            {
                breakpoint: 1600,
                settings: {
                    slidesToShow: 6
                },
            },
            {
                breakpoint: 1366,
                settings: {
                    slidesToShow: 5
                },
            },
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4
                },
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 3
                },
            }
        ]

    });
    $('.doctors-list > .content').slimScroll({
        height: '420px',
        size: '10px',
        position: 'right',
        color: '#2fc67b',
        distance: '20px',
        railVisible: true,
        railColor: '#dedede',
        alwaysVisible: true,
        railOpacity: 1,
        wheelStep: 120,
        allowPageScroll: false,
        // disableFadeOut: true,

    });


});

// var input = document.getElementById( 'file-upload' );
// var infoArea = document.getElementById( 'file-upload-filename' );
// var infoArea2 = document.getElementById( 'file-upload-filename2' );
//
//
// input.addEventListener( 'change', showFileName );
//
// function showFileName( event ) {
//
//     // the change event gives us the input it occurred in
//     var input = event.srcElement;
//
//     // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
//     var fileName = input.files[0].name;
//
//     // use fileName however fits your app best, i.e. add it into a div
//     infoArea.textContent = fileName;
//     infoArea2.textContent = fileName;
//
//     $('.file .section-submit').addClass('button').addClass('button__rounded').addClass('button--green');
//     var oldText = $('.file .section-submit').text();
//     var newText = $('.file .section-submit').data('text');
//     $('.file .section-submit').text(newText).data('text',oldText);
//
// }




function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object

    // files is a FileList of File objects. List some properties.
    var output = [];
    for (var i = 0, f; f = files[i]; i++) {

        console.log(f.size);

        let wynik = f.size;
        output.push('<li>', escape(f.name), ' - ',
            (wynik / 1024 / 1024).toFixed(2), ' (Mb)',
            // f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : 'n/a',
            '<a class="remove-element-file"><img src="img/x-red.svg"></a></li>');



        $('.file .section-submit').addClass('button').addClass('button__rounded').addClass('button--green');
        var oldText = $('.file .section-submit').text();
        var newText = $('.file .section-submit').data('text');
        var oldTextName = $('.file .name').text();
        var newTextName = $('.file .name').data('text');
        $('.file .section-submit').text(newText).data('text');
        $('.file .name').text(newTextName);
        $('.upload-visible').addClass('uploaded');
        $('p.info').hide();
        $('html, body').animate({
            scrollTop: $('.file').offset().top - 20
        }, 'slow');

    }
    $('#list #uploaded').append(output.join(''));

    $('#list2 #uploaded2').append(output.join(''));
    // document.getElementById('list2').innerHTML = '<ul class="uploaded">' + output.join('') + '</ul>';


}

document.getElementById('files').addEventListener('change', handleFileSelect, false);



//INPUT TYPE NUMBER
jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up"><img src="img/arrow-up.svg" alt=""></div><div class="quantity-button quantity-down"><img src="img/arrow-down.svg" alt=""></div></div>').insertAfter('.quantity input');
jQuery('.quantity').each(function() {
    var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

    btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
            var newVal = oldValue;
        } else {
            var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
    });

    btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
            var newVal = oldValue;
        } else {
            var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $('.date .dropdown-item').click(function(e) {
        e.preventDefault();

        var text = $(this).html();

        $(this).parents('.dropdown').find('.dropdown-toggle').html(text);
    });

    $('.submited-info').on('click', '.remove', function(e) {
        e.preventDefault();

        if ( $(this).closest('.values').children().length > 1 ) {
            $(this).parent().remove();
        } else {
            $(this).closest('.item').remove();
        }
    });

    $(document).on('click', '.edit', function (e) {
        $(this).parents('.form-row').nextAll().find('.section').removeClass('visited').removeClass('active');
        $(this).parents('.form-row').nextAll().find('.submited-info').hide();

    })


});